# inbuild python imports

# inbuilt django imports
from django.conf.urls import url, patterns

# third party imports

# inter-app imports

# local imports
from . import views



urlpatterns = [
	url(r'^robot/$', views.RobotView.as_view(), name = 'robot_view'),
]