# inbuild python imports
# inbuild python imports

# inbuilt django imports
from django.db import models

# third party imports
from mongoengine import *

# inter-app imports

# local imports


class RobotModel(Document):
	name = StringField(required = True)
	age = IntField(required = True)