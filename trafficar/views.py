# inbuild python imports

# inbuilt django imports
from django.shortcuts import render
from django.views.generic import View
from django.http.response import HttpResponse

# third party imports

# inter-app imports

# local imports
from .models import RobotModel


class RobotView(View):

	def get(self, request, *args, **kwargs):
		return HttpResponse('Hello World, I am a robot view')

	def post(self, request, *args, **kwargs):
		post_data = {
			'name' : request.POST.get('name'),
			'age' : int(request.POST.get('age'))
		}

		robot_instance = RobotModel.objects.create(**post_data)
		robot_instance.save()

		return HttpResponse('Created!')