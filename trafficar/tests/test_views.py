# inbuild python imports

# inbuilt django imports
from django.test import SimpleTestCase

# third party imports
from rest_framework import status

# inter-app imports
from worktoarch.utils import MongoTestCase

# local imports


class TestRobotView(MongoTestCase):

    def setUp(self):
        self.url = '/trafficar/robot/'

        self.post_data = {
        	'name' : 'Arpit',
        	'age' : 25
        }

    def test_first_view_url(self):
    	response = self.client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_request_data_in_database(self):
    	response = self.client.post(self.url, self.post_data)

    	self.assertEqual(response.status_code, status.HTTP_200_OK)
