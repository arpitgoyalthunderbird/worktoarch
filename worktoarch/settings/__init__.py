# inbuild python imports
import sys, os

# inbuilt django imports

# third party imports

# inter-app imports

# local imports


LOCAL_ENV = os.environ.get('IS_LOCAL_ENVIRONMENT')


if LOCAL_ENV == 'True':
	from development import *
else:
	from production import *