# inbuild python imports

# inbuilt django imports

# third party imports
import mongoengine

# inter-app imports

# local imports
from .development import *


mongoengine.connection.disconnect()

TEST_MONGO_SETTINGS = {
	'DB_NAME' : 'test',
	'HOST' : 'localhost',
	'PORT' : 27017,
	'USERNAME' : '',
	'PASSWORD' : ''
}

mongoengine.connect(TEST_MONGO_SETTINGS['DB_NAME'], host = TEST_MONGO_SETTINGS['HOST'], port = TEST_MONGO_SETTINGS['PORT'], username = TEST_MONGO_SETTINGS['USERNAME'], password = TEST_MONGO_SETTINGS['PASSWORD'])