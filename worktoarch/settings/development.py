# inbuild python imports

# inbuilt django imports

# third party imports
from mongoengine import connect

# inter-app imports

# local imports
from .settings import *

for conn, attrs in MONGO_SETTINGS.items():
	connect(attrs['DB_NAME'], conn, host = attrs['HOST'], port = attrs['PORT'], username = attrs['USERNAME'], password = attrs['PASSWORD'])