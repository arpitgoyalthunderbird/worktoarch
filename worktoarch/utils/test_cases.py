# inbuild python imports

# inbuilt django imports
from django.test import SimpleTestCase
from django.conf import settings

# third party imports
from mongoengine.connection import connect, disconnect, get_connection, get_db

# inter-app imports

# local imports


class MongoTestMixin(SimpleTestCase):
	"""
	Mixin to enforce use of mongodb, instead of relational database, in testing
	"""
	CONNECTION = get_connection()

	try:
		MONGO_DB_SETTINGS = settings.TEST_MONGO_SETTINGS
	except:
		raise AttributeError('settings file has no attribute TEST_MONGO_SETTINGS. Specify TEST_MONGO_SETTINGS in settings file.')

	def _pre_setup(self):
		"""
		This is presetup function calling the MongoTestMixin setup function because it is different than the original function
		"""
		SimpleTestCase._pre_setup(self)
		MongoTestMixin._setup_database(self)

	def _post_teardown(self):
		SimpleTestCase._post_teardown(self)
		MongoTestMixin._teardown_database(self)

	def _setup_database(self):
		disconnect()
		connect(self.MONGO_DB_SETTINGS['DB_NAME'], host = self.MONGO_DB_SETTINGS['HOST'], port = self.MONGO_DB_SETTINGS['PORT'])

	def _teardown_database(self):
		self.CONNECTION.drop_database(self.MONGO_DB_SETTINGS['DB_NAME'])
		disconnect()


class MongoTestCase(MongoTestMixin):
	pass
