#!/usr/bin/env python

import os, sys, subprocess

arguments = sys.argv

if len(arguments) < 2:
	raise Exception('Please pass the filename or directory structure as well')

file_path = arguments[-1]
append_mode = True if '-a' in sys.argv else False

def create_file(file_path, append_mode = False):
	if os.path.isfile(file_path) and not append_mode:
		print('file already exists!!')
		sys.exit()

	if not append_mode:
		subprocess.call(['touch', file_path])

	add_statements_to_file(file_path)

	success_message = 'file successfully modified' if append_mode else 'file successfully created'
	print(success_message)

def add_statements_to_file(file_path):
	import_statements = ['inbuild python imports', 'inbuilt django imports', 'third party imports', 'inter-app imports', 'local imports']

	file_content = ''
	for import_statement in import_statements:
		file_content += "# {0}\n{1}".format(import_statement, os.linesep)

	with open(file_path, 'r') as file:
		original_file_content = file.read()
		final_content = file_content + os.linesep + os.linesep + original_file_content

	with open(file_path, 'w') as file:
		file.write(final_content)


create_file(file_path, append_mode)


